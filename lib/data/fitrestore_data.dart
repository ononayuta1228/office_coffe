Map<String, dynamic> historyData(String title, DateTime now, int price){


  Map<String, dynamic> data = {
    "title": title,
    "createdAt": now,
    "price": price,
  };

  return data;
}