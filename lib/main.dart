import 'package:flutter/material.dart';
import 'package:office_coffee/screens/buy_finish_view.dart';
import 'package:office_coffee/screens/order_history_view.dart';

import 'screens/buy_view.dart';
import 'screens/delete_history_view.dart';
import 'screens/top_view.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        '/': (_) => new TopView(),
        '/history': (_) => new OrderHistoryView(),
        '/delete':(_) => new DeleteHistoryView(),
        '/buy/finish':(_) => new BuyFinishView(),

      },
      theme: ThemeData(
        primaryColor: Colors.white,
      ),
    );
  }
}

