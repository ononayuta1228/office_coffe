import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';


/// FirestoreDB操作系
// データ登録
void setData(String collection, Map data) {
  Firestore.instance.collection(collection).document().setData(data);
}


// データ一括削除
void deleteData(String collection,) {
  Firestore.instance.collection(collection).getDocuments().then((snapshot) {
    for (DocumentSnapshot ds in snapshot.documents){
      ds.reference.delete();
    }
  });
}



/// 業務ロジック系
int calcAmountBenefit(AsyncSnapshot<QuerySnapshot> snapshot,) {
  int sum = 0;
  snapshot.data.documents.forEach((item) {
    if(item["price"] == 80){
      sum += 20;
    }else if(item["price"] == 90){
      sum += 25;
    }

  });
  return sum;
}


