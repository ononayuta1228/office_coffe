import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:office_coffee/service/firestore_service.dart';


class OrderHistoryView extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('購入履歴画面',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        centerTitle: true,
      ),
      body: _historyList(),
    );
  }

  Widget _historyList() {
      return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('users').orderBy('createdAt', descending: true).snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) return Text("loading");
          return SingleChildScrollView(
            padding: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                Text('総購入数 : ' + snapshot.data.documents.length.toString()),
                Text('総利益 : ' + calcAmountBenefit(snapshot).toString()),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex:4,
                      child: Text('購入時間')
                    ),
                    Expanded(
                      flex:1,
                      child: Text('金額')
                    ),
                  ],
                ),
                ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children:
                    snapshot.data.documents.map((doc) {
                        return Row(
                          children: <Widget>[
                            Expanded(
                              flex:4,
                              child: Text(DateFormat("yyyy/MM/dd HH:mm:ss").format(doc.data["createdAt"].toDate()).toString())
                            ),
                            Expanded(
                              flex:1,
                                child: Text(' ¥' + doc.data["price"].toString())
                            ),
                          ],
                        );
                      },
                    ).toList(),
                ),
              ],
            ),
          );
        },
      );
  }
}
