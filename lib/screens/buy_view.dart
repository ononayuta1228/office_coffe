

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:office_coffee/data/fitrestore_data.dart';
import 'package:office_coffee/service/firestore_service.dart';


class BuyView extends StatelessWidget {
  BuyView(this.price);
  int price;

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('購入確認画面'),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '¥ ' + '$price',
              style: TextStyle(
                fontSize: 40,
              ),
            ),
            Text(
              'をお賽銭箱、もしくはPayPayにてお支払いください',
              textAlign: TextAlign.center,
            ),
            Text(
              '※お釣りはお賽銭からとってください',
              textAlign: TextAlign.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Image.asset('asset/saisen.png'),
                      ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Image.asset('asset/nayuta_QR.png'),
                      ),
                    ),
                  ),
              ],
            ),
            Container(
              child: RaisedButton(
                color: Colors.blue,
                onPressed: () {
                  setData("users", historyData("test",DateTime.now(),price));
                  Navigator.of(context).pushNamed('/buy/finish');
                },
                child: Text('　　　購入する　　　',
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white
                  ),
                ),
              ),
            ),
            Container(
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('戻る',
                  style: TextStyle(fontSize: 14),
                ),
              ),
            ),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
