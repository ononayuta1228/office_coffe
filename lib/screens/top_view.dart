import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:office_coffee/data/fitrestore_data.dart';
import 'package:office_coffee/service/firestore_service.dart';

import 'buy_view.dart';


class TopView extends StatefulWidget {
  @override
  TopViewState createState() => TopViewState();
}



class TopViewState extends State<TopView> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('注文画面'),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                        '紙コップ必要：¥90',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                        '紙コップ不要：¥80',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => BuyView(90)),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Image.asset('asset/useCup.png'),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.green, width: 4),
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => BuyView(80)),
                        );
                      },
                      // タッチ検出対象のWidget
                      child: Container(
                        padding: EdgeInsets.all(5),
                        child: Image.asset('asset/noCup.png'),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.pink, width: 4),
                          borderRadius: BorderRadius.circular(10),

                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),

            Container(
              padding: const EdgeInsets.all(10.0),
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/history');
                },
                child: Text('購入履歴一覧画面に遷移',
                  style: TextStyle(fontSize: 14),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushNamed('/delete');
        },
        child: Icon(Icons.delete),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
